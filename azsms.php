<?php
/**
 * Plugin Name: AZSMS
 * Plugin URI: https://azsms.vn
 * Description: Form send sms by azsms
 * Version: 2.6.2
 * Author: AzsmsTeam
 * Author URI: http://azsms.vn
 */
define('AZSMS_PLUGIN_DIR', plugin_dir_path(__FILE__));
require_once(AZSMS_PLUGIN_DIR . 'inc/admin/settingsCF.php');
require_once(AZSMS_PLUGIN_DIR . 'inc/admin/settingsWOO.php');
require_once(AZSMS_PLUGIN_DIR . 'inc/admin/settingAPI.php');
require_once(AZSMS_PLUGIN_DIR . 'inc/sent/config.php');


add_action('admin_enqueue_scripts', function ($hook) {
    $azsms_settings['codeEditor'] = wp_enqueue_code_editor(array('type' => 'text/css'));
    wp_localize_script('jquery', 'azsms_settings', $azsms_settings);
    wp_enqueue_script('wp-theme-plugin-editor');
    wp_enqueue_style('wp-codemirror');
});
