<?php

class SettingAzsms
{

    private $options;

    public function __construct()
    {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    public function add_plugin_page()
    {
        add_options_page(
            'Settings API AZSMS',
            'Settings API AZSMS',
            'manage_options',
            'azsms-api-setting-admin',
            array($this, 'create_admin_page')
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option('azsms_option_name');
        ?>
        <div class="wrap">
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('azapi_option_group');
                do_settings_sections('azsms-api-setting-admin');
                submit_button();
                ?>
            </form>
            <div class="note">
                <h2>Trạng thái gửi tin nhắn</h2>
                <textarea style="width: 800px;height: 400px"  id="fancy-textarea"><?= file_get_contents(AZSMS_PLUGIN_DIR.'logs/logs-azsim.txt');?></textarea>
            </div>
        </div>
        <script>
            jQuery(document).ready(function($) {
                wp.codeEditor.initialize($('#fancy-textarea'), azsms_settings);
            })</script>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'azapi_option_group', // Option group
            'azsms_option_name', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Cấu hình API AZSMS.VN', // Title
            array($this, 'print_section_info'), // Callback
            'azsms-api-setting-admin' // Page
        );

        add_settings_field(
            'apiazsms',
            'API AZSMS',
            array($this, 'api_callback'),
            'azsms-api-setting-admin',
            'setting_section_id'
        );
        add_settings_field(
            'simazsms',
            'GỬI TỪ SIM ( ví dụ sim 1 nhập vào ô số 1)',
            array($this, 'api_sim_callback'),
            'azsms-api-setting-admin',
            'setting_section_id'
        );
    }

    public function sanitize($input)
    {
        $new_input = array();
        if (isset($input['apiazsms']))
            $new_input['apiazsms'] = sanitize_text_field($input['apiazsms']);
        if (isset($input['simazsms']))
            $new_input['simazsms'] = sanitize_text_field($input['simazsms']);
        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Nhập api azsms.vn cấp:';
    }

    public function api_callback()
    {
        printf(
            '<input type="text" id="apiazsms" name="azsms_option_name[apiazsms]" value="%s" style="width:400px"/>',
            isset($this->options['apiazsms']) ? esc_attr($this->options['apiazsms']) : ''
        );

    }

    public function api_sim_callback()
    {

        printf(
            '<input type="number" id="simazsms" name="azsms_option_name[simazsms]" value="%s"/>',
            isset($this->options['simazsms']) ? esc_attr($this->options['simazsms']) : ''
        );

    }
}

if (is_admin())
    new SettingAzsms();