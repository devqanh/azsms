<?php

class WC_Settings_AZSMS
{

    public static function init()
    {
        add_filter('woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50);
        add_action('woocommerce_update_options_settings_tab_azsms', __CLASS__ . '::update_settings');
        add_action('woocommerce_settings_tabs_settings_tab_azsms', __CLASS__ . '::settings_tab');


    }

    public static function add_settings_tab($settings_tabs)
    {
        $settings_tabs['settings_tab_azsms'] = __('Settings AZSMS', 'azsms');
        return $settings_tabs;
    }

    public static function settings_tab()
    {
        woocommerce_admin_fields(self::get_settings());
    }

    public static function update_settings()
    {
        woocommerce_update_options(self::get_settings());
    }

    public static function get_settings()
    {

        $settings = array(

            'section_title' => array(
                'name' => __('Cấu hình gửi sms', 'azsms'),
                'type' => 'title',
                'desc' => '',
                'id' => 'wc_settings_azsms_section_title'
            ),

            'section_status'=>array(
                'title'    => __( 'Bật tắt tính năng gửi sms', 'azsms' ),
                'desc'     => __( 'Bật/Tắt', 'azsms' ),
                'id'       => 'wc_settings_azsms_status',
                'type'     => 'checkbox',
                'default'  => 'no',
            ),
            'description' => array(
                'name' => __('Cấu hình gửi sms khi khách hàng đặt hàng thành công', 'azsms'),
                'type' => 'textarea',
                'desc' => __('Cấu hình email hoặc tên của khách hàng bằng cú pháp | %%firstname%% | %%email%% | %%lastname%% ', 'azsms'),
                'id' => 'wc_settings_azsms_description',
                'css' => 'min-width: 80%; height: 300px;',
            ),

        );

        return apply_filters('wc_settings_azsms_settings', $settings);
    }

}

WC_Settings_AZSMS::init();

