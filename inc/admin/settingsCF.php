<?php
if (!defined('ABSPATH')) {
    exit;
}
add_filter('wpcf7_editor_panels', function ($panels) {
    $panels['azsms-settings-panel'] = array(
        'title' => __('Setings AZSMS', 'azsms'),
        'callback' => 'azsms_settings_func',
    );
    return $panels;
});
function azsms_settings_func($post)
{
    ?>
    <h2><?php echo esc_html(__('AZSMS Settings', 'azsms')); ?></h2>
    <div class="note">
        <p>Nếu trong form có trường your-name thì hiển thị tên bằng <strong>%%name%%</strong></p>
        <p>Nếu trong form có trường your-email thì hiển thị tên bằng <strong>%%email%%</strong></p>
        <p>Trong phần form bắt buộc có trường [tel* your-phone] để hệ thống gửi đến số điện thoại</p>
    </div>
    <fieldset>
        <legend><?php echo esc_html(__('Cấu hình gửi sms', 'azsms')); ?></legend>
        <textarea id="wpcf7-additional-settings" name="wpcf7-content-azsms" cols="100" rows="8" class="large-text"
                  data-config-field="additional_settings.body"><?= get_post_meta($post->id(), 'wpcf7_content_azsms', true); ?></textarea>
    </fieldset>
    <style>
        .note {
            font-style: italic;
        }

        .note strong {
            color: red;
        }
    </style>
    <?php
}

add_action('save_post', function ($post_id) {
    if (array_key_exists('wpcf7-content-azsms', $_POST)) {
        update_post_meta(
            $post_id,
            'wpcf7_content_azsms',
            $_POST['wpcf7-content-azsms']
        );
    }
});


