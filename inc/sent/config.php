<?php
add_action('wpcf7_mail_sent', function ($cf7) {
    $wpcf7 = WPCF7_ContactForm::get_current();
    $form_id = $wpcf7->id;
    $submission = WPCF7_Submission::get_instance();
    if ($submission) {
        $posted_data = $submission->get_posted_data();
        $email = !empty($email = $posted_data['your-email']) ? $email : null;
        $name = !empty($name = $posted_data['your-name']) ? $name : null;
        $phone = !empty($phone = $posted_data['your-phone']) ? $phone : null;
        if (!empty($content = get_post_meta($form_id, 'wpcf7_content_azsms', true)) && !empty($phone)) {

            $contentPutSms = str_replace(['%%name%%', '%%email%%'], [$name, $email], $content);
            // send sms api
            sendApiazsms($phone, $contentPutSms);
            // log content cf7
            file_put_contents(AZSMS_PLUGIN_DIR . 'logs/log-cf7.txt', $contentPutSms);
        }
    }
});

add_action('woocommerce_thankyou', function ($order_id) {
    $order = wc_get_order($order_id);
    if (!empty($content = get_option('wc_settings_azsms_description')) && get_option('wc_settings_azsms_status') == 'yes') {
        $firstname = !empty($firstname = $order->get_shipping_first_name()) ? $firstname : '';
        $lastname = !empty($lastname = $order->get_shipping_last_name()) ? $lastname : '';
        $email = !empty($email = $order->get_billing_email()) ? $email : '';
        if (!empty($order->get_billing_phone())) {
            $phone = $order->get_billing_phone();
            $contentPutSms = str_replace(['%%firstname%%', '%%lastname%%', '%%email%%'], [$firstname, $lastname, $email], $content);
            // send sms api
            sendApiazsms($phone, $contentPutSms);

            // log content woo

            file_put_contents(AZSMS_PLUGIN_DIR . 'logs/logs-woo.txt', $contentPutSms);
        }
    }
}, 10, 1);


function sendApiazsms($phone, $content)
{
    $dataConfig = get_option('azsms_option_name');
    $content = urlencode($content);
    $sim = !empty($sim = $dataConfig['simazsms']) ? $sim : 1;
    $phoneProcess = detect_number_azsms($phone);
    if (!empty($keyapi = $dataConfig['apiazsms']) && $phone) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://azsms.vn/api/send?key=$keyapi&phone=$phoneProcess&message=$content&sim=$sim",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
    } else {
        $response = "Vui lòng cập nhật api";
    }
    if ($response) {
        //file_put_contents(AZSMS_PLUGIN_DIR . 'logs/logs-azsim.txt', $response);
        addLogs(AZSMS_PLUGIN_DIR . 'logs/logs-azsim.txt', $response);
    }

}

function detect_number_azsms($number)
{
    $number = str_replace([' ', '+', '.', '(', ')'], '', $number);

    if (substr($number, 0, 2) == '84') {
        $number = '0' . substr($number, 2, strlen($number) - 2);
    }
    if (substr($number, 0, 2) == '00') {
        $number = substr($number, 2, strlen($number) - 2);
    }

    preg_match_all('/(84|0[3|5|7|8|9])+([0-9]{8,9})\b/m', $number, $matches, PREG_SET_ORDER, 0);

    if ($matches) {
        return '+84' . $number;
    }
    return false;
}

function addLogs($fileName, $line, $max = 30)
{
    $file = array_filter(array_map("trim", file($fileName)));

    // Make Sure you always have maximum number of lines
    $file = array_slice($file, 0, $max);

    count($file) >= $max and array_shift($file);

    array_push($file, $line);

    // Save Result
    file_put_contents($fileName, implode(PHP_EOL, array_filter($file)));
}